import { useState } from 'react'
import useEvent, { dispatch } from 'react-use-event'

export type LoginEvent = {
  type: 'login'
  token: string
}
export type LogoutEvent = {
  type: 'logout'
}

dispatch<LogoutEvent>('logout', {})

export function LoginPage() {
  const dispatch = useEvent<LoginEvent>('login')
  function login() {
    // fetch ...
    dispatch({ token: '123' })
  }
  return (
    <div>
      <button onClick={login}>login</button>
    </div>
  )
}

export function TopBar() {
  const [hasLogin, setHasLogin] = useState(false)
  useEvent<LoginEvent>('login', event => {
    event.token
    setHasLogin(true)
  })
  useEvent<LogoutEvent>('logout', () => setHasLogin(false))
  return (
    <div>{hasLogin ? <button>Logout</button> : <button>Login</button>}</div>
  )
}
