import { createContext, useCallback, useContext, useState } from 'react'
import jwtDecode from 'jwt-decode'

export type AuthState = LoginState | null

export type LoginState = {
  user: { id: number; email: string }
  token: string
}

type AuthContextValue = Partial<LoginState> & {
  login: (state: LoginState) => void
  logout: () => void
}

const AuthContext = createContext<AuthContextValue | null>(null)

function initialState(): AuthState {
  let token = localStorage.getItem('token')
  if (!token) return null
  return { token, user: jwtDecode(token) }
}

export function AuthProvider() {
  const [state, setState] = useState<AuthState>(initialState)

  const login = useCallback((state: LoginState) => {
    localStorage.setItem('token', state.token)
    setState(state)
  }, [])

  const logout = useCallback(() => {
    localStorage.removeItem('token')
    setState(null)
  }, [])

  return (
    <AuthContext.Provider
      value={{ ...state, login, logout }}
    ></AuthContext.Provider>
  )
}

export function useAuth() {
  const context = useContext(AuthContext)
  if (!context) {
    throw new Error(
      'useAuth() should be wrapped inside <AuthProvider></AuthProvider>',
    )
  }
  return context
}

const a = () => {
  const auth = useAuth()
  auth.user
  auth.token
  auth.login({token:'',user:{id:1,email:''}})
  auth.logout()
}
