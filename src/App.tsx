import React from 'react'
import logo from './logo.svg'
import './App.css'
import useStorageState, { createMemoryStorage } from 'react-use-storage-state'

let memoryStorage = createMemoryStorage()

function useBlur() {
  type State = false | string
  const [target, setTarget] = useStorageState<State>(
    'blur',
    false,
    memoryStorage,
  )
  return {
    target,
    setTarget,
    className: target ? ' blur ' : '',
  }
}

function Header() {
  const blur = useBlur()
  type Button = {
    text: string
  }
  const buttons: Button[] = [{ text: '1' }, { text: '2' }, { text: '3' }]
  return (
    <fieldset>
      <legend className={blur.className}>Header</legend>
      {buttons.map(button => (
        <button
          key={button.text}
          onClick={() => blur.setTarget(button.text)}
          className={button.text === blur.target ? '' : blur.className}
        >
          {button.text}
        </button>
      ))}
    </fieldset>
  )
}
function Body() {
  const blur = useBlur()
  return (
    <fieldset className={blur.className}>
      <legend>Body</legend>
    </fieldset>
  )
}
function Footer() {
  const blur = useBlur()
  return (
    <fieldset className={blur.className}>
      <legend>Footer</legend>
    </fieldset>
  )
}

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  )
}

export default App
